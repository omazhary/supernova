# Weapons

Below are the basic various types of weapons an adventurer can use in the field. The weapons listed here are based on the player's handbook table of weapons under Equipment in Chapter 5.

## Basic Weapons

<table>
    <tr>
        <th>Weapons</th>
        <th>Cost</th>
        <th>Damage</th>
        <th>Notes</th>
    </tr>
    <tr>
        <td colspan="4"><em>Simple Melee</em></td>
    </tr>
    <tr>
        <td>Club/Baton</td>
        <td>&Phi; 1</td>
        <td>1d4 Bludgeoning</td>
        <td>Light</td>
    </tr>
    <tr>
        <td>Dagger / Knife / Kunai</td>
        <td>&Phi; 2</td>
        <td>1d4 Piercing</td>
        <td>Finesse / Light / Thrown / Range ( 20 / 60 )</td>
    </tr>
    <tr>
        <td>Greatclub</td>
        <td>&Phi; 2</td>
        <td>1d8 Bludgeoning</td>
        <td>Two-handed / Heavy</td>
    </tr>
    <tr>
        <td>Handaxe</td>
        <td>&Phi; 5</td>
        <td>1d6 Slashing</td>
        <td>Light / Thrown / Range ( 20 / 60 )</td>
    </tr>
    <tr>
        <td>Javelin</td>
        <td>&Phi; 2</td>
        <td>1d6 Piercing</td>
        <td>Thrown / Range ( 30 / 120 )</td>
    </tr>
    <tr>
        <td>Hammer</td>
        <td>&Phi; 2</td>
        <td>1d4 Bludgeoning</td>
        <td>Light / Thrown / Range ( 20 / 60 )</td>
    </tr>
    <tr>
        <td>Mace</td>
        <td>&Phi; 5</td>
        <td>1d6 Bludgeoning</td>
        <td></td>
    </tr>
    <tr>
        <td>Quarterstaff</td>
        <td>&Phi; 2</td>
        <td>1d6 Bludgeoning</td>
        <td>Versatile ( 1d8 )</td>
    </tr>
    <tr>
        <td>Sickle</td>
        <td>&Phi; 1</td>
        <td>1d4 Slashing</td>
        <td>Light</td>
    </tr>
    <tr>
        <td>Spear</td>
        <td>&Phi; 1</td>
        <td>1d6 Piercing</td>
        <td>Versatile ( 1d8 ) / Thrown / Range ( 20 / 60 )</td>
    </tr>
    <tr>
        <td colspan="4"><em>Simple Ranged</em></td>
    </tr>
    <tr>
        <td>Dart / Shuriken</td>
        <td>&Phi; 1</td>
        <td>1d4 Piercing</td>
        <td>Finesse / Thrown / Range ( 20 / 60 )</td>
    </tr>
    <tr>
        <td>Sling</td>
        <td>&Phi; 1</td>
        <td>1d4 Bludgeoning</td>
        <td>Ammo / Range ( 30 / 120 )</td>
    </tr>
    <tr>
        <td>Light Crossbow</td>
        <td>&Phi; 25</td>
        <td>1d8</td>
        <td>Loading / Two-handed / Range ( 80 / 320 )</td>
    </tr>
    <tr>
        <td colspan="4"><em>Martial Melee</em></td>
    </tr>
    <tr>
        <td>Battleaxe</td>
        <td>&Phi; 10</td>
        <td>1d8 Slashing</td>
        <td>Versatile ( 1d10 ) </td>
    </tr>
    <tr>
        <td>Flail</td>
        <td>&Phi; 10</td>
        <td>1d8 Bludgeoning</td>
        <td></td>
    </tr>
    <tr>
        <td>Glaive</td>
        <td>&Phi; 20</td>
        <td>1d10 Slashing</td>
        <td>Heavy / Reach / Two-handed</td>
    </tr>
    <tr>
        <td>Greataxe</td>
        <td>&Phi; 30</td>
        <td>1d12 Slashing</td>
        <td>Heavy / Two-handed</td>
    </tr>
    <tr>
        <td>Greatsword</td>
        <td>&Phi; 50</td>
        <td>2d6 Slashing</td>
        <td>Heavy / Two-handed</td>
    </tr>
    <tr>
        <td>Halberd</td>
        <td>&Phi; 20</td>
        <td>1d10 Slashing</td>
        <td>Heavy / Reach / Two-handed</td>
    </tr>
    <tr>
        <td>Lance</td>
        <td>&Phi; 10</td>
        <td>1d12 Piercing</td>
        <td>Reach / Special ( You have disadvantage when you use a lance to attack a target within 5 feet of you. Also, a lance requires two hands to wield when you aren’t mounted. )</td>
    </tr>
    <tr>
        <td>Longsword</td>
        <td>&Phi; 15</td>
        <td>1d8 Slashing</td>
        <td>Versatile ( 1d10 ) </td>
    </tr>
    <tr>
        <td>Maul</td>
        <td>&Phi; 10</td>
        <td>2d6 Bludgeoning</td>
        <td>Heavy / Two-handed</td>
    </tr>
    <tr>
        <td>Morningstar</td>
        <td>&Phi; 15</td>
        <td> 128 Piercing</td>
        <td></td>
    </tr>
    <tr>
        <td>Pike</td>
        <td>&Phi; 5</td>
        <td>1d10 Piercing</td>
        <td>Heavy / Reach / Two-handed</td>
    </tr>
    <tr>
        <td>Rapier</td>
        <td>&Phi; 25</td>
        <td>1d8 Piercing</td>
        <td>Finesse</td>
    </tr>
    <tr>
        <td>Scimitar</td>
        <td>&Phi; 25</td>
        <td>1d6 Slashing</td>
        <td>Finesse / Light</td>
    </tr>
    <tr>
        <td>Shortsword</td>
        <td>&Phi; 10</td>
        <td>1d6 Piercing</td>
        <td>Finesse / Light</td>
    </tr>
    <tr>
        <td>Trident</td>
        <td>&Phi; 5</td>
        <td>1d8 Piercing</td>
        <td>Versatile ( 1d8 ) / Thrown ( 20 / 60 )</td>
    </tr>
    <tr>
        <td>Warpick</td>
        <td>&Phi; 5</td>
        <td>1d8 Piercing</td>
        <td></td>
    </tr>
    <tr>
        <td>Warhammer</td>
        <td>&Phi; 15</td>
        <td>1d8 Bludgeoning</td>
        <td>Versatile ( 1d10 )</td>
    </tr>
    <tr>
        <td>Whip</td>
        <td>&Phi; 2</td>
        <td>1d4 Slashing</td>
        <td>Finesse / Reach</td>
    </tr>
    <tr>
        <td colspan="4"><em>Martial Ranged</em></td>
    </tr>
    <tr>
        <td>Blowgun</td>
        <td>&Phi; 10</td>
        <td>1 Piercing</td>
        <td>Range ( 25 / 100 ) / Loading</td>
    </tr>
    <tr>
        <td>Hand Crossbow</td>
        <td>&Phi; 75</td>
        <td>1d6 Piercing</td>
        <td>Light / Range ( 30 / 120 ) / Loading</td>
    </tr>
    <tr>
        <td>Heavy Crossbow</td>
        <td>&Phi; 50</td>
        <td>1d10 Piercing</td>
        <td>Heavy / Range ( 100 / 400 ) / Loading / Two-handed</td>
    </tr>
    <tr>
        <td>Longbow</td>
        <td>&Phi; 50</td>
        <td>1d8 Piercing</td>
        <td>Range ( 150 / 600 )</td>
    </tr>
    <tr>
        <td>Net</td>
        <td>&Phi; 1</td>
        <td></td>
        <td>Thrown / Range ( 5 /15 ) / Special: A Large or smaller creature hit by a net is restrained until it is freed. A net has no effect on creatures that are formless, or creatures that are Huge or larger. A creature can use its action to make a DC 10 Strength check, freeing itself or another creature within its reach on a success. Dealing 5 slashing damage to the net (AC 10) also frees the creature without harming it, ending the effect and destroying the net. When you use an action, bonus action, or reaction to attack with a net, you can make only one attack regardless of the number of attacks you can normally make.</td>
    </tr>
    <tr>
        <td colspan="4"><em>Ballistic Weapons</em></td>
    </tr>
    <tr>
        <td>Semi-automatic Pistol</td>
        <td>&Phi; 30</td>
        <td>1d4 Piercing X 2</td>
        <td>Light / Loading / Range ( 30 / 20 )</td>
    </tr>
    <tr>
        <td>Automatic Pistol</td>
        <td>&Phi; 60</td>
        <td>1d4 Piercing X 4</td>
        <td>Light / Loading / Range ( 30 / 20 ) / Recoil</td>
    </tr>
    <tr>
        <td>Shotgun ( n - barrels )</td>
        <td>&Phi; 45</td>
        <td>1d6 Piercing X n</td>
        <td>Loading / Recoil / Pump / Range ( 15 / 30 )</td>
    </tr>
    <tr>
        <td>Semi-automatic Shotgun</td>
        <td>&Phi; 70</td>
        <td>1d6 Piercing X 2</td>
        <td>Loading / Recoil / Pump / Range ( 15 / 30 )</td>
    </tr>
    <tr>
        <td>Automatic Shotgun</td>
        <td>&Phi; 85</td>
        <td>11d6 Piercing X 4</td>
        <td>Loading / Recoil / Pump / Range ( 15 / 30 )</td>
    </tr>
    <tr>
        <td>SMG ( Auto )</td>
        <td>&Phi; 120</td>
        <td>1d4 Piercing X 4</td>
        <td>Light / Loading / Recoil / Range ( 30 / 120 )</td>
    </tr>
    <tr>
        <td>Assault Rifle ( Auto )</td>
        <td>&Phi; 200</td>
        <td>1d8 Piercing X4</td>
        <td>Heavy / Loading / Recoil / Range ( 100 / 400 )</td>
    </tr>
    <tr>
        <td>Sniper Rifle</td>
        <td>&Phi; 400</td>
        <td>1d12 Piercing</td>
        <td>Heavy / Loading / Single Fire / Range ( 500 / 1000)</td>
    </tr>
    <tr>
        <td colspan="4"><em>Energy Weapons</em></td>
    </tr>
    <tr>
        <td>Pistol</td>
        <td>&Phi; 35</td>
        <td>1d4 X 2</td>
        <td>Light / Loading / Range ( 60 / 120 )</td>
    </tr>
    <tr>
        <td>Rifle</td>
        <td>&Phi; 220</td>
        <td>1d8 X 4</td>
        <td>Heavy / Loading / Range (140 / 400 )</td>
    </tr>
    <tr>
        <td>Sniper Rifle</td>
        <td>&Phi; 500</td>
        <td>4d12</td>
        <td>Heavy / Loading / Single Fire / Range ( 600 / 1000 )</td>
    </tr>
</table>
