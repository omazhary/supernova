# Armors

Below are the various basic types of armor an adventurer can use in the field.
The armors listed here are based on the player's handbook table of armors under
Equipment in Chapter 5.

## Basic Armors

<table>
    <tr>
        <th>Armor</th>
        <th>Cost</th>
        <th>AC Calculation</th>
        <th>Notes</th>
    </tr>
    <tr>
        <td colspan="4"><em>Light</em></td>
    </tr>
    <tr>
        <td>Padded Vest</td>
        <td align="right">&Phi; 5</td>
        <td>11 + DEX</td>
        <td>Disadvantage on stealth checks</td>
    </tr>
    <tr>
        <td>Leather Vest</td>
        <td align="right">&Phi; 10</td>
        <td>11 + Dex</td>
        <td></td>
    </tr>
    <tr>
        <td>Kevlar Vest</td>
        <td align="right">&Phi; 45</td>
        <td>12 + Dex</td>
        <td></td>
    </tr>
    <tr>
        <td colspan="4"><em>Medium</em></td>
    </tr>
    <tr>
        <td>Multi-Layered Kevlar</td>
        <td align="right">&Phi; 10</td>
        <td>12+ Dex (+2 Max)</td>
        <td></td>
    </tr>
    <tr>
        <td>Titanium Mesh</td>
        <td align="right">&Phi; 50</td>
        <td>13 + Dex (+2 Max)</td>
        <td></td>
    </tr>
    <tr>
        <td>Titanium Scale</td>
        <td align="right">&Phi; 50</td>
        <td>14 + Dex (+2 Max)</td>
        <td>Disadvantage on stealth Checks</td>
    </tr>
    <tr>
        <td>Ceramic Plate</td>
        <td align="right">&Phi; 400</td>
        <td>14 + Dex (+2 Max)</td>
        <td></td>
    </tr>
    <tr>
        <td>Plate + Kevlar Weave</td>
        <td align="right">&Phi; 750</td>
        <td>15 + Dex (+2 Max)</td>
        <td>Disadvantage on stealth Checks</td>
    </tr>
    <tr>
        <td colspan="4"><em>Heavy</em></td>
    </tr>
    <tr>
        <td>Titanium Dipped Kevlar</td>
        <td align="right">&Phi; 30</td>
        <td>14</td>
        <td>Disadvantage on stealth checks</td>
    </tr>
    <tr>
        <td>Titanium Dipped Bi-Kevlar Weave</td>
        <td align="right"> &Phi; 75</td>
        <td>16</td>
        <td>
            <ul>
                <li>Disadvantage on stealth checks</li>
                <li>Strength Requirement 13</li>
            </ul>
        </td>
    </tr>
    <tr>
        <td>Titanium Dipped Bi-Kevlar Weave with flexible plating</td>
        <td align="right">&Phi; 200</td>
        <td>17</td>
        <td>
            <ul>
                <li>Disadvantage on stealth checks</li>
                <li>Strength Requirement 15</li>
            </ul>
        </td>
    </tr>
    <tr>
        <td>Titanium Dipped Tri-Kevlar Weave with flexible plating</td>
        <td align="right">&Phi; 1500</td>
        <td>18</td>
        <td>
            <ul>
                <li>Disadvantage on stealth checks</li>
                <li>Strength Requirement 15</li>
            </ul>
        </td>
    </tr>
    <tr>
        <td colspan="4"><em>Shields</em></td>
    </tr>
    <tr>
        <td>Retractable Titanium</td>
        <td align="right">&Phi; 10</td>
        <td>AC +2</td>
        <td></td>
    </tr>
    <tr>
        <td>Energy Shield</td>
        <td align="right">&Phi; 50</td>
        <td>12 + DEX</td>
        <td>Against energy weapons</td>
    </tr>
</table>
