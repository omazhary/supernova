# The Anitana

The Anitana one of the playable races in the Supernova setting. They hail from
the planet **Charak-5** in Ograni Republic space.

## Appearance

The Anitana are of a smaller size compared to the rest of the races in the
galaxy. While still humanoid, they have evolved to adapt to their subterranean
environment. Their skin is covered with thick, sturdy fur, and their hands have
evolved claws to better help them move underground. While they cannot see as
well as other races in direct sunlight, they make up for it with an especially
acute sense of hearing.

## Traits

**Ability Score Increase:** Increase one of your ability scores by +2, and
another of your ability scores by +1.

**Age:** The typical Anitana is considered mature after 36 cycles. They can live
to be as old as 400 years by human standards.

**Size:** An Anitana stands between 3 and 4 feet tall, with an average weight of
100 to 150 pounds. Your size is small.

**Speed:** You base walking speed is 25 feet.

## Biological Abilities

**Blindsight:** Anitana can perceive their surroundings using echolocation up to
a distance of 120 feet. They cannot use this skill if they are deafened.
Instead, they have to rely on their regular sight, which extends only to 60
feet in poorly lit conditions, and 30 feet in well lit areas. Anitana have
disadvantage on perception checks that rely on regular sight.

**Earth Glide:** An Anitana can burrow through non magical, unworked earth and
stone. While doing so, the Anitana does not disturb the material it moves
through. The Anitana can glide up to 40 feet per turn.

**Non-Visual Insights:** Because of an Anitana's primary dependence on senses
other than sight to perceive their surroundings, they have learned to evaluate
other creatures' dispositions based on much more than just visual cues. You have
advantage on insight checks made against other creatures using your
echolocation.
