# The Nachsyrr

The Nachsyrr are one of the playable races in the Supernova setting. They hail
from the dark caves of **Obsyrr-16** in Monarchy space.

## Appearance

The Nachsyrr are a taller version of their Holsyrr kin, albeit with a grey-ish
brown skin tone. Due to evolving underground, they had to replicate sunlight
artificially, to varying degrees of intensity. Instead of the bark-like
striations that decorate a Holsyrr's skin, Nachsyrr instead have developed what
seems to be rock-like patterns along their bodies. This allows them to blend
with existing rock-like formations, and-in some rare cases-fuse with them.

## Traits

**Ability Score Increase:** Increase one of your ability scores by +2, and
another of your ability scores by +1.

**Age:** The typical Holsyrr is considered mature after 29 cycles. They can
live to be as old as 450 years by human standards.

**Size:** A Nachsyrr stands between 5 and 6 feet tall, with an average weight of
120 to 170 pounds. Your size is medium.

**Speed:** You base walking speed is 30 feet.

## Biological Abilities

**Rock-like Exterior:** You have resistance against non-magical bludgeoning,
piercing, and slashing damage. Once per short rest, you can reduce any incoming
non-magical bludgeoning, piercing, or slashing damage by 1d12 + your character
level.

**Natural Camouflage:** Because of their skin tone and natural patterns, a
Nachsyrr can blend effortlessly against any rocky formation, or against
surfaces that are of the same color and have similar patterns. You have
advantage on stealth checks made when trying to conceal yourself in such
locations.

**Nature's Engineer:** Because of how attuned to nature you are, you are never
without a weapon or tool as long as you have access to rocks. As a bonus action,
you can grow a tool or simple weapon from a rocky surface at the cost of
1d6 + your level. The higher your overall level, the better engineered a
construct is.

**Sunlight Sensitivity:** You are accustomed to internal artificial lighting.
When faced with direct sunlight, you get disadvantage on all checks that require
sight, as well as attack rolls and saving throws.
