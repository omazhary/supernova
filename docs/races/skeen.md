# The S'keen

The S'keen are one of the playable races in the Supernova setting. They hail
from the planet **Rai'tur** in the Oppala belt.

## Appearance

S'keen are large, humanoid reptilian creatures who have evolved to survive on
the harsh conditions of their home planet. A typical S'keen ranges between 7 to
8 feet tall, and are sturdily built. Their skin is covered with scales of
varying colours based on their lineage and where on Rai'tur they grew up, which
also influences what biological defences they have developed.

## Traits

**Ability Score Increase:** Increase one of your ability scores by +2, and
another of your ability scores by +1.

**Age:** The typical S'keen is considered mature after 25 cycles on Rai'tur.
This is used as a normalized maturity age across hierarchy-controlled worlds.

**Size:** A S'keen stands between 7 and 8 feet tall, with an average weight of
250 pounds. Your size is medium.

**Speed:** You base walking speed is 30 feet.

## Biological Abilities

**Darkvision:** A typical S'keen is able to see in the dark thermal outlines of
living creatures (and possibly objects), to a distance of 60 feet. However, this
ability does not work when subject to a high ambient temperature.

**Breath Weapon:** As a way to protect themselves from predators, a S'keen can
spew a blast of a particular type of element, based on their heritage as
dictated by the the color of their scales. They also are resistant to that
element's damage.

| Scale Color | Damage Type/Resistance | Breath Weapon |
|-------------|------------------------|---------------|
| Black | Acid | 5 by 30 ft. line (DEX save) |
| Blue | Lightning | 5 by 30 ft. line (DEX save) |
| Green | Poison | 15 ft. cone (CON save) |
| Red | Fire | 15 ft. cone (DEX save) |
| White | Cold | 15 ft. cone (CON save) |

Breath weapon save DC = 8 + CON + Proficiency Bonus
