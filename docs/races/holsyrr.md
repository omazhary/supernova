# The Holsyrr

The Holsyrr are one of the playable races in the Supernova setting. They hail
from the lush forests of **Obsyrr-1** in Monarchy space.

## Appearance

The Holsyrr are a lithe, humanoid species that seem more tree than creature at
first glance. Their skin is riddled with long natural bark-like striations while
their eyes are pupil-less, with a warm golden glow. Their skin colors range from
dark brown to bright green depending on the type of forest they inhabit.

## Traits

**Ability Score Increase:** Increase one of your ability scores by +2, and
another of your ability scores by +1.

**Age:** The typical Holsyrr is considered mature after 21 cycles. They can
live to be as old as 650 years by human standards.

**Size:** A Hochsyrr stands between 5 and 6 feet tall, with an average weight of
120 to 170 pounds. Your size is medium.

**Speed:** You base walking speed is 30 feet.

## Biological Abilities

**Photosynthesis:** As long as a Holsyrr character is in direct sunlight, they
can consume water to heal themselves. You can drink some water as a bonus action
to restore 1d8 + 1 HP. At tenth level, the amount of HP restored is 2d4 + 2. You
can use this ability a number of times equal to your CON modifier before needing
a long rest.

**Nerves of Wood:** Because of how different your nervous system is to that of
other races, you can empathically feel what plants in your vicinity do.
Furthermore, this ability gives you advantage against effects that attempt to
charm you or manipulate your will.

**Nature's Engineer:** Because of how attuned to nature you are, you are never
without a weapon or tool as long as you have access to a tree or some other form
of vegetation. As a bonus action, you can grow a tool or simple weapon from a
plant at the cost of 1d6 + your level. The higher your overall level, the better
engineered a construct is.

**Heart of Wood:** You are vulnerable to fire damage.
