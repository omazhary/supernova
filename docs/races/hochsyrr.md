# The Hochsyrr

The Hochsyrr are one of the playable races in the Supernova setting. They hail
from the mountaintops of **Obsyrr-25** in Monarchy space.

## Appearance

The Hochsyrr are a lithe, humanoid species that are more avian than human in
their appearance. It is said that the higher in the mountains a Hochsyrr lives,
the more pronounced their avian likeness is. Majestic feathered wings decorate
their backs.

## Traits

**Ability Score Increase:** Increase one of your ability scores by +2, and
another of your ability scores by +1.

**Age:** The typical Hochsyrr is considered mature after 19 cycles. They can
live to be as old as 85 years by human standards.

**Size:** A Hochsyrr stands between 5 and 6 feet tall, with an average weight of
120 to 170 pounds. Your size is medium.

**Speed:** You base walking speed is 30 feet.

## Biological Abilities

**Flight:** Using their wings, a Hochsyrr can fly with a speed equal to their
walking speed. However, they cannot fly while wearing medium or heavy armor.

**Wind Vortex:** Starting at 3rd level, you can use your wings to produce an
effect similar to the spell [Gust of Wind](http://dnd5e.wikidot.com/spell:gust-of-wind).
You can do so only once per long rest. You can use Intelligence, Wisdom, or
Charisma for the purposes of calculating the spell's save DC upon character
creation.

**Farsight:** You can see further than the average humanoid, and in
significantly more detail. You have advantage on perception checks that rely on
sight, and using ranged weapons beyond their normal range does not impose a
disadvantage on your attacks.

**Avian Frame:** You are vulnerable to non-magical bludgeoning damage.
