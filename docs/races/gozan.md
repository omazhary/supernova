# The Gozan

The Gozan is one of the playable races in the Supernova setting. They hail from
an underground city in the bowels of **Kezkin-12** in Untethered space.

## Appearance

The Gozan are an insect-like race that developed mostly underground, but have
been known to venture to the forests of the surface every now and then to forage
for nutrition and search for resources. The typical Gozan stand between 5 and 6
feet tall, and weight around 100 to 150 pounds. Their exoskeletons, while
durable, shield a somewhat vulnerable interior, and they communicate primarily
using the antennae on their heads which give them limited psionic powers. They
also have a lesser developed pair of arms sprouting from their torso which they
tend to keep hidden under their clothes.

## Traits

**Ability Score Increase:** Increase one of your ability scores by +2, and
another of your ability scores by +1.

**Age:** The typical Gozan is considered mature after 10 cycles. They can live
to be as old as 60 years by human standards.

**Size:** A Gozan stands between 5 and 6 feet tall, with an average weight of
100 to 150 pounds. Your size is medium.

**Speed:** You base walking speed is 30 feet.

## Biological Abilities

**Wall Crawling:** A Gozan can walk up walls and ceilings with the same walking
speed they do as if walking on the ground.

**Compound Eyes:** A Gozan has near 360&#176; vision. Consequently, they can
never be surprised while awake.

**Minor Psionics:** A Gozan can typically use its antennae to communicate
telepathically with other creatures. However, doing so opens them up to all
surrounding thoughts. If no attempt to focus is made when using this skill, or
if an attempt is made and failed (WIS save against 10 + crowd size), the Gozan
takes 1d6 points of psychic damage.
