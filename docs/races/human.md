# Humans / Terrans

Humans are one of the playable races in the Supernova setting. Terrans hail from
Earth, whereas non-Terran humans are scattered around the various planets and
systems of Untethered space.

## Appearance

No race in the galaxy is as diverse and varied as the human race is. Especially
now that some humans live their entire lives in space, evolution has impacted
them in a myriad of different ways. Terrans (humans who originate from Earth)
are typically between 5 to 6 feet tall, and can weigh anywhere between 120 to
250 pounds. Generations of humans born in space, however, can grow to be between
6 and 8 feet tall, and can weigh anywhere between 100 and 180 pounds.

## Traits

**Ability Score Increase:** Increase one of your ability scores by +2, and
another of your ability scores by +1.

**Age:** The typical Terran is considered mature after 18 years. They can live
to be as old as 120 years. The typical human is considered mature after 15
years, and can live to be as old as 80 years.

**Size:** A Terran stands between 5 and 6 feet tall, with an average weight of
120 to 250 pounds. A Human stands between 6 and 8 feet tall, with an average
weight of 100 and 180 pounds. Your size is medium.

**Speed:** You base walking speed is 30 feet.

## Biological Abilities

**Versatility:** You can become proficient in two skills of your choosing.

**Specialization:** You can pick a feat from the [available feats](http://dnd5e.wikidot.com/#toc68).
