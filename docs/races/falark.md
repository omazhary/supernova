# The Fal'ark

The Fal'ark one of the playable races in the Supernova setting. They hail from
a collection of planetoids orbiting **Maxim-6** in Untethered space.

## Appearance

The typical Fal'ark stands between 8 and 10 feet tall. With their massive
humanoid frame, pale grey skin, protruding canines, and red eyes, a Fal'ark can
be extremely intimidating. While smooth, their skin evolved to be as thick as
armor.

## Traits

**Ability Score Increase:** Increase one of your ability scores by +2, and
another of your ability scores by +1.

**Age:** The typical Fal'ark is considered mature after 15 cycles. They can live
to be as old as 80 years by human standards.

**Size:** A Fal'ark stands between 8 and 10 feet tall, with an average weight of
250 to 300 pounds. Your size is medium.

**Speed:** You base walking speed is 30 feet.

## Biological Abilities

**Thick Skin:** Fal'ark have evolved an armor-like skin to shield them against
the meteoric hailstorms on their home planetoids. You base AC is calculated as
13 + DEX as opposed to the default value. You are also resistant to bludgeoning,
piercing, and slashing damage from non-magical sources.

**Weapon of Mouth:** A Fal'ark can use their protruding canines to inflict
damage on their opponents. Instead of a weapon attack, you can-instead-bite your
opponent. The attack roll is determined by either your STR or DEX, and you deal
1d8 + either your STR or DEX modifiers depending on which you chose for the
attack roll.

**Terrifying Visage:** Most people are terrified of the Fal'ark due to their
appearance. As such, Fal'ark have a hard time establishing a social rapport with
members of other races. You have a disadvantage on persuasion and deception
checks against members of other races, but you have advantage on intimidation
checks.
