---
sidebar_position: 1
---

# Introduction

Aristocracy and nobility are more than what define the Monarchy. At its core,
the Obsyyr Monarchy is strives towards a single ideal: purity in all things.
Purity from corruption, purity from pollution, even from industrialized
technologies. In the mid 25th century, Obsyyr elders recognized that the only
way to accomplish purity, is through a firm hand, and an unwavering, unchanging
rule.

## Society and Internal Politics

As the name implies, the Obsyyr are ruled over by a monarch. A royal line that
extends all the way back to the 25th century. The first Monarch, Obsyyr the
first, was chosen by the council of elders due to his vision, his clarity of
purpose, and-most importantly-his unshakable belief in the harmonious
coexistence of creature and environment. His beliefs are carried on by his most
recent successor, Obsyyr the seventh.

Obsyyrian society is strictly separated based on race. Dogma ranks the different
castes on how well they coexist with a planet's natural environment, and how
much they allow it to influence them. At the top of the hierarchy are two of the
three dominant races in the Monarchy: the Holsyrr and the Nachsyrr. The holsyrr
are native to the lush forests of the Monarchy's home planet Obsyrr-1, whereas
the Nachsyrr are native to the subterranean cave systems of Obsyrr-2. Further
down the caste ladder are the Hochsyrr, native to the mountaintops of Obsyrr-25.
Finally, the Salash are considered the bottom of the caste system, due to their
mixed ancestry from human and Obsyrr parents. Needless to say, the further down
a caste is ranked, the easier it is to justify negative action against them.

## Technology

Obsyyrians derive their devices and vehicles from the environment around them.
Devices are grown by archdruids to suit specific needs for which there is no
natural solution. Similarly, vehicles are grown completely from Abadar mother
trees in a completely organic and harmonious process that returns as much to
nature as it takes.

## Relationships with Other Factions

The monarchy can be classified as xenophobic depending on their perception of
another race's integration with nature. However, they are currently in a mutual
defence pact with the galactic core out of pure necessity. Loathe as they are to
admit it, because of their focus on harmonious natural co-existence, their
military forces leave a lot to be desired. They recognize they need support from
other factions against the S'keen threat, at the very least until they build
their forces to the point where they become self-sufficient.

## Member Races

There are 4 playable races in the Obsyyr Monarchy:

- [The Holsyrr](../../races/holsyrr)
- [The Nachsyrr](../../races/nachsyrr)
- [The Hochsyrr](../../races/hochsyrr)
- [The Salash](../../races/salash)
