# Introduction

More a loose collection of groups than an organized faction, the Untethered
systems are fiercely independent, and live their lives according to their
internal code. While the diversity of value systems, more often than not, leads
to confrontations with other groups, they have always united against threats
beyond their borders.

## Society and Internal Politics

Consisting primarily of three groups, these groups are bound solely by a loosely
defined oath of loyalty to the Untethered coalition. There exists no definable
system of governance, and each group is expected to govern itself as it sees
fit.

The main groups that comprise the Untethered systems consist of:

- **Void Raiders:** A group of primarily Fal'ark pirates who attack
  non-coalition ships passing through untethered space.
- **The Chroniclers:** A group of primarily Gozan that have settled in
  Untethered space to continue chronicling the history of galaxy free from the
  potential bias of operating in a system with a ruling body.
- **The Technomages:** A group of wizards who fled to Untethered space to avoid
  persecution by races and casters who consider their combination of technology
  and magic perverse.

Void raiders are roaming bands of marauders who travel shipping lanes and
smuggling routes in search of a new prize. While they initially started out as
solely consisting of Fal'ark tribes, as the Untethered systems grew, these
tribes have welcomed new members from different races into their ranks.

The Chroniclers have a commune on Sigma-Canis-38, and routinely dispatch roving
members to observe various parts of the galaxy to document civilizations or
civilization-shaping events. Being a Chronicler simply requires passing an
initiation, and a dedication to unbiased, unfiltered, history.

The Technomages were originally descendants of humans who fled Earth when it
seemed that was doomed to climate change and the damage to the atmosphere during
the third world war. With rudimentary space flight technology, their colony ship
stumbled across the Obsyrr Monarchy's territory, where they were put to work as
slaves. However, it was there that they were introduced to the concept of magic
as practiced by the Obsyrr. The few that managed to flee the Obsyrr worlds found
themselves in Untethered space. It was there that they started the first
technomage coven, and merged magic and technology together. In fact, it is most
likely that any enchanted tool, weapon, or item one comes across can be traced
back to one of the now many covens in Untethered space. Over the years,
technomages have accepted members of other races into their covens, as a way to
incorporate more technologies into their arsenals, and to ensure the continuity
of their practice.

## Technology

Devices and vehicles in the Untethered Systems are a mishmash of technologies
found throughout the galaxy. Void raiders will often incorporate looted ships
and weaponry into their fleets, or bring appropriated items to technomages for
enchantment.

## Relationships with Other Factions

The Untethered Systems border three of the five factions: the S'keen, the
Galactic Core, and the Ograni Collective. All factions, however, have a negative
relationship with the Untethered Systems. The S'keen view them as a disorganized
rabble that require "_enlightenment_". The Ograni consider them a pack of
smugglers, thieves, and pirates who jeopardize their business interests.
The Obsyrr view their existence as a threat to the balance between nature and
creature. Finally, the Terrans (formerly humans) in the Galactic Core consider
the humans in the Untethered Systems race traitors for abandoning the planet.
While the Untethered Systems do not think of any of the other factions in a
particularly negative light, for a denizen of the Untethered Systems no trek is
as unpleasant as one that takes them outside Untethered space.

## Member Races

Over the years, the Untethered systems have grown to include much more than just
the Fal'ark, the Gozan, and the Humans. Through no effort of their own, they are
seen as a haven for those considered to be on the wrong side of either the law,
or society. For simplicity, only the founding races are listed below:

- [The Fal'ark](../../races/falark)
- [The Gozan](../../races/gozan)
- [The Humans](../../races/human)
