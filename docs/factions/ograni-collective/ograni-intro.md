# Introduction

Master crafters and traders, the Ograni Collective is fortunate to have most of
the galaxy's _Element X_ (also known as "ex") deposits, the galaxy's main source
for refined omnigel. The collective has optimized the extraction, refinement,
and export of their omnigel stock to all willing to pay its exorbitant prices.

## Society and Internal Politics

The Ograni collective is a collection of mercantile guilds that have each
dominated an aspect of extracting, refining, and exporting omnigel. What at
first was three races warring over each other's resources and territory has
become a solid whole driven primarily by profit. The Ognasi of planet Vensik-3,
the Rutyr of Sarys-12, and the Anitana of Charak-5 have now become immensely
wealthy and prosperous.

Ograni society is organized into mercantile guilds (or houses) that manage the
three aspects of element X extraction, refinement, and export as omnigel. The
racial separation that existed before has now been removed. So much so, that the
houses allow non-Ograni races to join by way of apprenticeship contracts.
Apprenticeship contracts are legally (and potentially more) binding, and are
extremely skewed depending on the house the contract is formed with. They range
from the standard 10-year unpaid apprenticeship, and can be as exploitative as
signing away one's soul to the head of their house.

The houses as they currently are in the 31st millennium:

- **House Vitar** (formerly House Kezack) handles element X extraction
- **House Karabazahn** (formerly House Zhur) handles element X refinement
- **House Okkul** handles omnigel exports

## Technology

Ograni technology is developed for a single purpose: to support the mining,
processing, and export of omnigel. As a result of the three houses working
together, all devices and vehicles share the same underlying technological
principles. The design aesthetic is also similar, being geared towards size over
comfort. Their ships are massive, to ship as much raw material as possible from
one location to another. Their tools are robust and efficient, with a lot of
effort being put into optimizing function over user friendliness or experience.

## Relationships with Other Factions

The Ograni Collective is generally on good terms on any faction that they
consider a current or prospective business partner. That includes the S'keen
hierarchy, to an extent. The Untethered Systems, however, are a different story.
Given that these consist primarily of pirates, smugglers, black market dealers,
or worse (technomages are widely disliked throughout the galaxy), the Collective
often mounts "_training exercises_" on their borders. The occasional training
"_mishap_" tends to coincide with the assassination of a prominent Fal'ark tribe
leader, or similar individuals in positions that threaten the Collective's
business.

## Member Races

Many members of non-founding races can immigrate to the collective for chances
at a more lucrative life once their apprenticeship contracts are done. However,
for simplicity, listed below are the founding races of the collective:

- [The Ognasi](../../races/ognasi)
- [The Ratyr](../../races/ratyr)
- [The Anitana](../../races/anitana)
