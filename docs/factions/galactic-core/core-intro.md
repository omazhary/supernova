---
sidebar_position: 1
---

# Introduction

One of the more welcoming and tolerant factions in the galaxy, the core systems
are, by far, the most diverse civilization based on how many races were grouped
together in a single entity. Its citizens enjoy freedoms afforded to them by the
galactic charter and guaranteed by the core fleets.

## Society and Internal Politics

As a society that strives towards a representative democracy as best it can, the
core systems have elected to create a single governing body (the core senate)
where each member race is represented by a single delegate. Decisions are voted
on in senate sessions, and the results of these decisions are carried out by the
various governmental bodies the senate oversees. Delegates are elected into the
office of senator of their respective species by virtue of self-managed
elections on their home planets.

On a societal level, individuals are given the freedom to do what they want, so
long as they are able to provide for themselves, and do not infringe on other
individuals' freedoms and rights. Law is maintained by the peacekeepers, an
interstellar police force that answers directly to the senate and that includes
recruits of all the member planets.

On a military level, the core systems no longer recognize individual member
planet militaries, rather, all militaristic resources have been pooled together
to form the all-powerful **Galactic Fleet**. The galactic fleet is deployed
where it is needed by senate vote, and primarily patrols the outer systems of
core space.

## Technology

To accommodate the diverse range of technologies developed by the member
systems, galactic core technologies are designed to adapt to as many
interconnectivity standards as possible. Given enough iterations, newly added
technologies are absorbed into the mainstream, and are fully incorporated into a
unified core asthetic and functional tendencies. This has resulted in devices
and vehicles that are highly adaptable to non-core technologies, while still
maintaining a sleek, smoothly engineered asthetic.

## Relationships with Other Factions

The galactic core strives to maintain peaceful relations with all other factions
as best it can. However, its member systems are still given to biases, grudges,
and resentments of their own. For instance, systems closer to the S'keen front
might have a difficult relationship with S'keen races that flee the hierarchy
looking for a different life in the core. Similarly, Salash groups in the core
might feel significant resentment towards the Obsyyr Monarchy for exiling and
systemically eradicating their collective races in the past.

However, on a macro scale, the core is generally allied with the Obsyyr Monarchy
in a mutual defence pact against any possible incursions from the S'keen. It
relies on the Ograni Republic to supply it with resources for technological
research and development, and generally has positive relations with all
factions.

## Member Races

Races in the galactic core are extremely diverse, ranging from the typical human
from Earth (now called Terra), to the spore-like hiveminds of Icta Prime. Below,
however, are the founding races:

- [Terrans](../../races/human)
- [Ratyr](../../races/ratyr)
- [Salash](../../races/salash)
