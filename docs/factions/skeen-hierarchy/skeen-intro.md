# Introduction

In a galaxy full of politics, negotiations, relationships, and betrayals, the
S'keen hierarchy is the only constant.
The hierarchy is a well oiled machine, each cog knowing with resolute conviction
their place in it.

## Society and Internal Politics

The hierarchy is a societal system that defines every aspect of a person's life.
It defines their place in society, what they can aspire to be in the future, and
it does so based primarily on a highly complex multi-dimensional mathematical
equation that takes into account:

- a person's ancestry,
- their aptitude for different skills as determined by a rigorous test
  (the Ku'tan) every hierarchy citizen has to take once they reach the age of
  maturity (25 cycles),
- their familial and interpersonal relationships, and
- the current needs of the hierarchy.

The Ku'tan has been in place for well over a millennium, and is widely considered
by hierarchy citizens as what has saved their society and culture from more than
a century of civil war.

The hierarchy is ruled over by a five-member council, known as the **Ikzyym**.
Each member of the council is promoted to their position as dictated by the
rules of the hierarchy itself which are determined primarily by the Ku'tan, and
dictate the line of succession.
Each of the five members represents a branch of government:

- Executive
- Legislative
- Judicial
- Strategy
- Morale

## Technology

The hierarchy is primarily a militaristic society, with most of its efforts
directed towards expanding outward as opposed to investing inward. As such, the
technology has adapted to suit that purpose. It boasts the most diverse
collection of technologies in the galaxy, because of how many systems it has
annexed. Devices and vessels take on a primarily hyper-mechanized, rugged look.
There is little emphasis on form or aesthetics, because all devices at some
point or another are supplemented with parts from other technologies.

## Relationships with Other Factions

Members of the hierarchy view races that have not been "_integrated_" into its
rigid system of values and beliefs as "_lesser_" races. They are to be shown the
**true** way of living their lives as dictated by the hierarchy, and integrated
into it. Often by military force. Consequently, the S'keen hierarchy finds
itself at odds with the rest of the galaxy. While it has been able to expand and
integrate other races into its way of life, they have run into three major
powers that have halted their progress beyond a certain border. These are:

- the Untethered systems,
- the Galactic Core systems, and
- the Obsyyr Monarchy.

The hierarchy has grown to begrudgingly respect the borders of these systems for
various reasons. While the latter two are considered somewhat equals to the
hierarchy due to the presence of some form of government, and because of their
tenuous alliance to each other which makes any military forays into their
territories ill-advised, the hierarchy cannot afford the same respect to the
untethered systems. From a hierarchy point of view, the untethered are chaotic,
have no determinable system of government, and consist primarily of marauders,
pirates, and other creatures of scrupulous dispositions. As far as the hierarchy
is concerned, the reason it has not yet been able to integrate them into itself
is because of the negative impact doing so would have on its military power,
potentially opening it up for an invasion from other neighbours.

## Member Races

While the races that live within the hierarchy are as varied as the number of
planets it controls, only the core race is provided here. Players may propose
other races and these may be added here based on how balanced they are, and how
well they fit the setting.

- [The S'keen](../../races/skeen)
